# Bookmarks

This repository was created to save my searching by internet 
for help and informations about somethinks, like Computer 
science, Gamming, Programming Languages, Computer Graphics,
Eletronic Engeneer, Physics, Mathematics, Chemistry and
some other about tecnologies and science

Use freely, and if you whish, add yours link to impruve this
list, or fix some broken link.

This under BSD License

[Nilton José Rizzo](http://www.rizzo.eng.br)
rizzo ( at ) rizzo ( dot ) eng ( dot ) br

## Technology

* [Brazilian FM Radios](pages/Radios.md)
* [Brasilian Laws](pages/BrasilianLaws.md)
* [Computer Generics](pages/ComputerGenerics.md)
* [Computer Graphics](pages/ComputerGraphics.md)
* [Databases](pages/Databases.md)
* [Datascience](pages/DataScience.md)
* [Drugstores in Brazil](pages/Drugstores.md)
* [Educations](pages/Educations.md)
* [Eletronics links](pages/Eletronics.md)
* [Events](pages/Events.md)
* [Free Stocks to Designers](pages/Design.md)
* [Generic](pages/Generic.md)
* [Latex Utilities](pages/Latex.md)
* [Math](pages/Math.md)
* [Multimidia](pages/Multimidia.md)
* [Museum](pages/Museum.md)
* [Network Tools and Docs](pages/Network.md)
* [Operational Systems](pages/OperationalSystems.md)
* [Others](pages/Others.md)
* [Portuguese Grammar](pages/PortugueseGrammar.md)
* [Programming Languages](pages/Languages.md)
* [Science](pages/Science.md)
* [Security](pages/Security.md)
* [Shells](pages/Shells.md)
* [Star Trek Fans](pages/StarTrek.md)
* [Symposiums](pages/Symposiums.md)
* [Tools](pages/Tools.md)
* [Unix Basic Commands](pages/UnixBC.md)
* [Utilities Links](pages/UtilityLinks.md)
* [Universities](pages/Univerities.md)
* [VPS Servers](pages/VPSservices.md)
* [Window Managements](pages/WindowManagement.md)

## Web Applications

* [Annotations](pages/Annotations.md)

## Religion

* [Spiritism](pages/Spiritism.md)
