# Graphics and Designs

## Free picture stocks

* [Pix Bay](https://pixabay.com/)
* [Open Clipart](https://openclipart.org/)
* [Freepik](https://www.freepik.com/)
* [Flaticon](https://www.flaticon.com/)
* [Seeklogo](https://seeklogo.com/)
* [Vexels](https://www.vexels.com/)
* [Vector 4 Free](https://www.vector4free.com/)
* [Vector Portal](https://www.vectorportal.com/)
* [Free Vector](https://www.freevectors.net/)
* [Online webfonts](https://www.onlinewebfonts.com/icon/)
* [Free SVG](https://freesvg.org/)
* [Unsplash](https://unsplash.com/)

