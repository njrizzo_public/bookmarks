# Utilities Links

* [IPV6 Test](https://ipv6-test.com/)
* [IP Ok](https://ipok.com.br/)
* [Into DNS](https://intodns.com/)
* [DNS Queries](https://www.dnsqueries.com/en/)
* [Flylib - Configuring IMAP server with imap-wu](https://flylib.com/books/en/3.326.1.223/1/)
* [Root Service - OpenDmarc](https://www.rootservice.org/de/howtos/freebsd/hosting_system/mailserver/opendmarc)
* [Dan's Blog](https://www.dan.me.uk/blog/2016/06/01/add-dkim-signing-to-freebsd-servers/)
* [Steve Jenkins](https://www.stevejenkins.com/blog/2015/03/installing-opendmarc-rpm-via-yum-with-postfix-or-sendmail-for-rhel-centos-fedora/)


* [Main page](../README.md)

