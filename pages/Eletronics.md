# Eletronics

* [Revistas sobre eletrônica](https://blogdopicco.blogspot.com/)
* [Eletronic Design - ThinerCad](https://www.tinkercad.com/)
* [Eletronic Design - Every Circuit](https://everycircuit.com/app)
* [Eletronic Simulation](http://falstad.com/circuit/e-capac.html)
* [Eletronic Design - DigiKey](https://www.digikey.com/schemeit/home)
* [Eletronic Simulation](https://simulator.io/board/I1yagTJYUd)
* [Eletronic Design - PartSim](https://www.partsim.com/simulator#)
* [Eletronic Design - EasyEda](https://easyeda.com/)
* [Eletronic Design - CircuitLab](https://www.circuitlab.com/)
* [RastBerry PI Projects](https://www.etechnophiles.com/raspberry-pi-pico-projects/)


* [Main page](../README.md)

