# Symposiums

## Internet of Thinks (IoT)

* [IoT Brasilian Forum](https://iotbrasil.org.br/)
* [Workshop and Symposiums](https://feirasecongressos.com.br/informatica/)
* [Capes Sucupira](https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/veiculoPublicacaoQualis/listaConsultaGeralPeriodicos.jsf)


## 2021

* [IEEE International Conference Conputer Vision](http://iccv2021.thecvf.com/home)
* [Engineering of Computer based Systems](https://ecbs-conference.org/)
* [IFFF CVPR](http://cvpr2021.thecvf.com/)
* [IEEE Symposium on Cuputer Applications & Industrial Eletronics](https://sites.google.com/site/iscaie2021/home)


* [Main page](../README.md)

