# Computer Graphics

* [Viola-Jones](https://medium.com/datadriveninvestor/understanding-and-implementing-the-viola-jones-image-classification-algorithm-85621f7fe20b)
* [Prof. Agostinho](https://agostinhobritojr.github.io/tutorial/pdi/#_prefácio)
* [Prof. Paulo e prof. Esperança](http://orion.lcg.ufrj.br/cg2/)
(http://www.ic.uff.br/~ferraz/TELPIII/OpenGL/Programacao%20OpenGL.pdf)
* [Prof. Inhaúma Neves Ferraz](http://www.ic.uff.br/~ferraz/TELPIII/OpenGL/)
* [Prof. J de Rezende](https://www.ic.unicamp.br/~rezende/CGSoftware.htm)
* [Prof. Jorge Cavalcanti](http://www.univasf.edu.br/~jorge.cavalcanti/)
* [Prof. Anselmo Montenegro](www.ic.uff.br/~anselmo/)

## OpenGL Tutorials

* [OpenGL Oficial website](https://www.khronos.org/opengl/)
* [Modern OpenGL Tutorials](http://ogldev.atspace.co.uk/index.html)
* [Prof. Jorge Cavalcante](https://docplayer.com.br/72485255-Computacao-grafica-opengl-01.html)


* [Main page](../README.md)

