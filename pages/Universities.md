# Brasilian Universities

## UFRJ - Federal University of Rio de Janeiro

* [Federal University of Rio de Janeiro](http://www.ufrj.br)
* [COPPE](http://www.coppe.ufrj.br)
* [Search at SEI system of UFRJ](http://portal.sei.ufrj.br/index.php/modulos-sei/pesquisa-publica)


## UFRRJ - Federal Rural University of Rio de Janeiro

* [Federal Rural University of Rio de Janeiro](http://www.ufrrj.br)


## UFF - Federal Fluminense University

* [Federal Fluminense University](http://www.uff.br)


* [Main page](../README.md)

