# Data Science

## Datasets

* [The World Bank](https://data.worldbank.org/)
* [Amazon](https://registry.opendata.aws/)
* [Microsoft](https://azure.microsoft.com/en-us/services/open-datasets/)
* [Kaggle](https://www.kaggle.com/datasets)
* [Google](https://datasetsearch.research.google.com/)


* [Back](../README.md)
