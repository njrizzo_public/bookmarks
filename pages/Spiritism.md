# Spiritism

Only in pt-BR

## Links úteis

* [FEB - Federação Espírita Brasileira](https://www.febnet.org.br/)
* [CEERJ - Conselho Espírita do Estado do Rio de Janeiro](https://www.ceerj.org.br)
* [Mundo Espírita](http://www.mundoespirita.com.br)
* [O Consolador](http://www.oconsolador.com.br/)
* [A Bíblia do Caminho](http://bibliadocaminho.com/)
* [Acervo Espírita](http://www.acervoespirita.com.br/)
* [Portal Kardec](http://www.portalkardec.com.br/Livros/Downloads/Download%20Livros.htm)

## Centros Espíritas

* [Centro Espírtia Isaac Lima](http://www.ceilrj.org.br)
* [Centro Espírita Luz e Verdade](https://celv.org.br/)
* [Centro Espírita Discipulos de Jesus](http://cedj.org.br/)


* [Main page](../README.md)

