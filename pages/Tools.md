# Tools

* [Grep](https://www.cyberciti.biz/faq/searching-multiple-words-string-using-grep/)
* [Site Point - Commons Commands](https://www.sitepoint.com/15-little-known-unix-commands/)
* [Git tips](https://tableless.com.br/tudo-que-voce-queria-saber-sobre-git-e-github-mas-tinha-vergonha-de-perguntar/)


## Graphics Tools

* [Canvas](https://www.canva.com)
* [Split images](https://pinetools.com/split-image)
* [Background remove](https://www.remove.bg)

## Stocks Free

* [Music](https://mixkit.co/free-stock-music/)


* [Main page](../README.md)

