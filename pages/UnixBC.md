# Unix Basics Commands

## Generics Tools

* [Modern Unix tools](https://github.com/ibraheemdev/modern-unix)

## FreeBSD man pages

* [Adding New Users - adduser](https://www.freebsd.org/cgi/man.cgi?adduser)
* [Copy Files - cp](https://www.freebsd.org/cgi/man.cgi?cp)
* [Display or Set date and Time](https://www.freebsd.org/cgi/man.cgi?date)
* [List Directory Contents - ls](https://www.freebsd.org/cgi/man.cgi?ls)
* [Make Directories - mkdir](https://www.freebsd.org/cgi/man.cgi?mkdir)
* [Move Files - mv](https://www.freebsd.org/cgi/man.cgi?mv)
* [Set the Date and Time Via NTP - ntpdate](https://www.freebsd.org/cgi/man.cgi?ntpdate)
* [Process Status - ps](https://www.freebsd.org/cgi/man.cgi?ps)
* [Modify a User's Password - passwd or ypasswd](https://www.freebsd.org/cgi/man.cgi?passwd)
* [Remove Directories Entries - rm](https://www.freebsd.org/cgi/man.cgi?rm)
* [Substitute User Identify - su](https://www.freebsd.org/cgi/man.cgi?su)
* [Manipulate Tape Archives - tar](https://www.freebsd.org/cgi/man.cgi?tar)
* [Display Information about Processes - top](https://www.freebsd.org/cgi/man.cgi?top)
* [Time Command Execution - time](https://www.freebsd.org/cgi/man.cgi?time)
* [Text Editor - ex, vi or view](https://www.freebsd.org/cgi/man.cgi?vi)


## Linux man pages

* [Adding New Users - adduser](https://linux.die.net/man/8/adduser)
* [Copy Files - cp](https://linux.die.net/man/1/cp)
* [Display or Set date and Time](https://linux.die.net/man/1/date)
* [List Directory Contentes - ls](https://linux.die.net/man/1/ls)
* [Make Directoriers - mkdir](https://linux.die.net/man/1/mkdir)
* [Move Files - mv](https://linux.die.net/man/1/mv)
* [Set the Date and Time Via NTP - ntpdate](https://linux.die.net/man/8/ntpdate)
* [Process Status - ps](https://linux.die.net/man/1/ps)
* [Modify a User's Password - passwd](https://linux.die.net/man/5/passwd)
* [Remove Directories Entries - rm](https://linux.die.net/man/1/rm)
* [Substitute User Identify - su](https://linux.die.net/man/1/su)
* [Manipulate Tape Archives - tar](https://linux.die.net/man/1/tar)
* [Display Information about Processes - top](https://linux.die.net/man/1/top)
* [Time Command Execution - time](https://linux.die.net/man/1/time)
* [Text Editor - ex, vi, vim, gvin or view](https://linux.die.net/man/1/vi)



* [Main page](../README.md)

