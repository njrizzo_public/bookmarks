# Others

* [Ibm Emulations](https://www.ibm.com/developerworks/br/library/os-emulatehistory/index.html)
* [Padim](https://www.padrim.com.br/)
* [Vakinha](https://www.vakinha.com.br/)
* [Adobe Color Circle](https://color.adobe.com/pt/create/color-wheel)

# Free Stocks

## Photos

* [PixaBay](https://pixabay.com/)
* [unDraw](https://undraw.co/)

# Online Tools

* [Canvas](https://www.canva.com/)


* [Main page](../README.md)

