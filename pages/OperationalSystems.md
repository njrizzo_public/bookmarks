# Operational Systems

## Unix like operational systems

* [DragonFly](pages/DragonFly.md)
* [FreeBSD](pages/FreeBSD.md)
* [Linux](pages/Linux.md)
* [NetBSD](pages/NetBSD.md)
* [OpenBSD](pages/OpenBSD.md)
* [Ubuntu](pages/Ubuntu.md)

## On-line tools 

* [Check some itens on server](http://dnsgoodies.com/index.htm)
* [ISPConfig](https://www.ispconfig.org/)


* [Main page](../README.md)

