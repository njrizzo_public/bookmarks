# Science

## New Qualis

* [New Qualis](https://ppgcc.github.io/discentesPPGCC/pt-BR/qualis/)
* [Spreadsheet to new Qualis](https://docs.google.com/spreadsheets/d/1vYzEllNS-OjmRQFvdnTUR4-y2Pq0BE9QjK5FH5eVA3M/edit#gid=1660128855)


## How to write a project

* [How to write a project](https://marcoarmello.wordpress.com/2012/03/13/projeto/)
* [What do you want from a research project]https://marcoarmello.wordpress.com/2014/07/09/expectativa/)
* [How to write the justification](https://marcoarmello.wordpress.com/2015/06/12/justificativaprojeto/)
* [How to write a extension project](https://oaprendizemsaude.wordpress.com/2015/11/12/projeto-de-extensao-parte-1-resumo-da-proposta/)

## Verbs used

* [Verbs to use for goals](https://www.conjugacao.com.br/verbos-para-objetivos/)
* [Verbs to use for goals 2](http://ienomat.com.br/administrasempre/index.php/2014/02/25/verbos-para-elaboracao-de-objetivos-gerais-e-especificos/)


* [Main page](../README.md)

