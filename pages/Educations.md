# Educations

## Undergraduate

## Graduate

* [Informatics in Educations](https://ieducacao.ceie-br.org/)



## Tools

* [Conceptboard](https://app.conceptboard.com/)
* [Google Classroom](https://classroom.google.com/)
* [Miro](https://miro.com/)
* [Moodle](http://moodle.org)
* [Padled](https://padlet.com)
* [Tagarelas Chat](https://tagarelas.chat)
* [FlowChart Builder](https://whimsical.com/)
* [Lucida Chart](https://www.lucidchart.com/)
* [Lucid Share board](https://lucid.app/)
* [Planning and analytics platform for K12 schools](https://www.chalk.com/)
* [GoCanqr](https://www.goconqr.com/)
* [Educreations](https://www.educreations.com/)
* [Prova Fácil](https://www.provafacilnaweb.com.br/)
* [Plagiarism checker](http://plagiarisma.net/)

* [Ranking de periódicos](https://www.scimagojr.com/journalrank.php)
* [Tempo de espera para publicação](https://scirev.org/)
* [Correção de textos online](https://languagetool.org/)
* [Fazer download de artigos de forma 100% aberta](https://doaj.org/)
* [Procure por locais para publicação 1](https://jane.biosemantics.org/)
* [Procure por locais para publicação 2](ou https://journalfinder.elsevier.com/)
* [Procure por locais para publicação 3](ou https://journalsuggester.springer.com/)
* [Fazer download de livros de graça e legalmente](https://www.gutenberg.org/)
* [Pesquise a origem de uma imagem](https://tineye.com/)


* [Main page](../README.md)

