# FreeBSD 

* [FreeBSD Oficial Site](https://freebsd.org)
* [Common Command List](https://www.ws.afnog.org/afnog2006/e0/ha/freebsd/freebsdref1.pdf)
* [FreeBSD Shop Zazzele](https://www.zazzle.com.br/store/shopfreebsd)

# FreeBSD Handbook

* [FreeBSD Handbook](https://docs.freebsd.org/en/books/handbook/)
* [FreeBSD Basics](https://docs.freebsd.org/en/books/handbook/basics/)

# FreeBSD as Workstation

* [Vermaden](https://vermaden.wordpress.com/freebsd-desktop/)


# FreeBSD on RaspBerry

* [VZaigrin](https://vzaigrin.wordpress.com/2014/04/18/working-with-gpio-on-raspberry-pi-with-freebsd/)


* [Main page](../README.md)

