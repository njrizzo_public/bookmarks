# Databases

## Sqlite

* [SQLite Forum](https://sqlite.org/forum/forummain)
* [SQLite Tutorial - Tutorials Point](https://www.tutorialspoint.com/sqlite/index.htm)
* [SQLite Tutorial - ZetCode](http://zetcode.com/db/sqlitec/)
* [SQLite Tutorial - Videlais](https://videlais.com/2018/12/12/c-with-sqlite3-part-1-downloading-and-compiling/)
* [SQLite Tutorial](https://www.sqlitetutorial.net/)

## Mysql

* [Mysql Tutorial](https://www.mysqltutorial.org/)

## Postgres

* [Postgres Tutorial](https://www.postgresqltutorial.com/)



* [Back](../README.md)
