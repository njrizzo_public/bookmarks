# Programming Languages

##

* [Prof. André Santanche - Learning Computer Programming](https://www.ic.unicamp.br/~santanch/teaching/alg/2012-1/)
* [Prof. Leandro](https://dcc.ufrj.br/~leandro/mat/xbt236/Lista1.pdf)
* [Prof. Paulo Roma](http://orion.lcg.ufrj.br/python/)

## Assembly

* [Assembly x86](https://silva97.gitbook.io/assembly-x86/)


## Mark Down

* [MarkDown Guide](https://www.markdownguide.org)
* [Git Hub](https://guides.github.com/features/mastering-markdown/)
* [Adam-p](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* [Daring Fireball](https://daringfireball.net/projects/markdown/syntax)
* [GFM](https://github.github.com/gfm/)
* [MicroSoftware](https://docs.microsoft.com/pt-br/contribute/how-to-write-use-markdown)


## C/C++

* [C/C++ Vartypes limits](https://www.tutorialspoint.com/c_standard_library/limits_h.htm)
* [C/C++ standart](https://en.cppreference.com)
* [C/C++ Feature Tests](https://en.cppreference.com/w/cpp/feature_test)
* [C/C++ Feature Test: Macro e Policies](https://isocpp.org/std/standing-documents/sd-6-sg10-feature-test-recommendations)
* [C/C++ Feature Test](https://github.com/makelinux/examples/blob/master/cpp/features.cpp)
* [C/C++ Standart](http://www.open-std.org/Jtc1/sc22/wg21/docs/papers/2005/n1804.pdf)
* [Spiral Rule](http://c-faq.com/decl/spiral.anderson.html)


## Generic 

* [W3 Resource](https://www.w3resource.com/)
* [Dev Mozilla - CSS Selectors](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors)

## Python

* [R Pubs](https://rpubs.com/gomes555/comandos_python)


* [Main page](../README.md)

