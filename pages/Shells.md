# Shells

## C Shell

* [C Shell Scripting](https://en.wikibooks.org/wiki/C_Shell_Scripting)
* [C Shell](https://www.grymoire.com/Unix/Csh.html)
* [C Shell Tutorial](http://web.eng.hawaii.edu/Tutor/csh.html)
* [Introduction to C Shell Programming](http://www-cs.canisius.edu/ONLINESTUFF/UNIX/shellprogramming.html#F)
* [The C Shell Tutorial](https://www2.cs.duke.edu/csl/docs/csh.html)
* [Unix Shell Diferences](http://www.faqs.org/faqs/unix-faq/shell/shell-differences/)
* [Wikipidia - C Shell](https://en.wikipedia.org/wiki/C_shell)
* [Writing C Shell Scripts](https://www.dur.ac.uk/resources/its/info/guides/3Cshells.pdf)

## Tenex C Shell (tcsh)

* [Tenex C Shell](https://learnxinyminutes.com/docs/tcsh/)
* [Wikipidia Tenex C Shell](https://en.wikipedia.org/wiki/Tcsh)
* [Learn X in Y minutes](https://learnxinyminutes.com/docs/tcsh/)


* [Main page](../README.md)


